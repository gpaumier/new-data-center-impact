## Deploying the site
1. [Install MyST](https://mystmd.org/guide/quickstart#installing-the-myst-markdown-cli).
2. Set the `BASE_URL` environment variable in your shell (e.g with [Fish](https://fishshell.com/), `set -x BASE_URL /~nshahquinn-wmf/new-data-center-impact/`).
3. Build the static site by running `myst build --html`.
4. Copy the generated files in `_build/html` to the docroot on your server (e.g. `rsync -a ./_build/html/ peopleweb.discovery.wmnet:~/public_html/new-data-center-impact`).