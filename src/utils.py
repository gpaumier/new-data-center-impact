from numbers import Number

import pandas as pd

from wmfdata.utils import sig_figs


def format_number(x):
    if isinstance(x, Number) and not pd.isnull(x):
        x = sig_figs(x, 3)
        M = 1_000_000
        G = 1_000_000_000
        
        if x < M:
            return "{:,.0f}".format(x)
        elif x < G:
            x_in_M = sig_figs(x / M, 3)
            return f"{x_in_M} M"
        else:
            x_in_G = sig_figs(x / G, 3)
            # I would like to use G here, but in my experience, people don't
            # necessarily understand what it means but do understand B for "billion".
            return f"{x_in_G} B"
    else:
        return x
