|          |   number of countries | first switch date   | last switch date   | yearly page views   |
|:---------|----------------------:|:--------------------|:-------------------|:--------------------|
| cohort 1 |                    33 | 2018-03-22          | 2018-04-05         | 39.3 B              |
| cohort 2 |                     5 | 2018-07-19          | 2018-07-19         | 1.13 B              |